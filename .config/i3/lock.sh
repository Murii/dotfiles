#!/bin/sh
set -e
xset s off dpms 0 10 0
i3lock --color=1e2021 --ignore-empty-password --show-failed-attempts --nofork
xset s off -dpms
