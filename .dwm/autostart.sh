nohup xsettingsd &
exec compton -b -f
nohup tlp start &
nohup redshift &
nohup conky &
nohup ../feh_random_background.sh &
nohup xautolock -time 10 -locker slock &
exec dwmblocks >> /dev/null/ &
