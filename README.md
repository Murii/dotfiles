
# Installation 
## Installing i3-gaps on Debian based system


sudo apt install libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev libxcb-icccm4-dev libyajl-dev libstartup-notification0-dev libxcb-randr0-dev libev-dev libxcb-cursor-dev libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev autoconf xutils-dev libtool


cd /tmp

sudo git clone https://github.com/Airblader/xcb-util-xrm

cd xcb-util-xrm

sudo git submodule update --init

sudo ./autogen.sh --prefix=/usr

sudo make

sudo make install

cd /some/directory

sudo git clone https://www.github.com/Airblader/i3 i3-gaps

cd i3-gaps

sudo autoreconf --force --install

sudo rm -rf build

sudo mkdir build

cd build

sudo ../configure --prefix=/usr --sysconfdir=/etc

sudo make

sudo make install

ONCE DONE:  
cd ~/.config/i3/
vim config

Add the following anywhere inside the config file:

```
#GAPS 
gaps inner 10
gaps outer 25
smart_gaps on
```

Then install some additional packages to make your desktop enjoyable 
sudo apt-get install compton hsetroot rxvt-unicode xsel rofi fonts-noto fonts-mplus xsettingsd lxappearance scrot viewnior neofetch xss-lock redshift conky termite

## Explanation for the installed packages

Compton is a compositor to provide some desktop effects like shadow, transparency, fade, and transiton.

Hsetroot is a wallpaper handler. i3 has no wallpaper handler by default.

URxvt is a lightweight terminal emulator, part of i3-sensible-terminal.

Xsel is a program to access X clipboard. We need it to make copy-paste in URxvt available. Hit Alt+C to copy, and Alt+V to paste.

Rofi is a program launcher, similar with dmenu but with more options.

Noto Sans and M+ are my favourite fonts used in my configuration.

Xsettingsd is a simple settings daemon to load fontconfig and some other options. Without this, fonts would look rasterized in some applications.

LXAppearance is used for changing GTK theme icons, fonts, and some other preferences.

Scrot is for taking screenshoot. I use it in my configuration for Print Screen button. I set my Print Screen button to take screenshoot using scrot, then automatically open it using Viewnior image viewer. 

Neofetch shows you the informations you see when you start a terminal.

## Prefered font in terminal

When it comes to fonts, I use Monospace Regular.

## For vim
cd ~/.vim/bundle/YouCompleteMe
python3 install.py --clangd-completer


## More info

Please check the i3-starterpack repo on my git.


